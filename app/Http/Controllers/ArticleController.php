<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function welcome()
    {
        $daftarKategori = Category::orderBy('updated_at', 'desc')->get();
        $daftarArtikel = Article::orderBy('updated_at', 'desc')->get();
        $daftarUsers = User::orderBy('updated_at', 'desc')->get();
        return view('welcome', ["daftarKategori"=>$daftarKategori,"daftarArtikel"=>$daftarArtikel,"daftarUsers"=>$daftarUsers]);

    }

    public function index()
    {
        $daftarKategori = Category::orderBy('updated_at', 'desc')->get();
        $daftarArtikel = Article::orderBy('updated_at', 'desc')->get();
        $daftarUsers = User::orderBy('updated_at', 'desc')->get();
        return view('dashboard', ["daftarKategori"=>$daftarKategori,"daftarArtikel"=>$daftarArtikel,"daftarUsers"=>$daftarUsers]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $article = new Article;
        $article->title = $request->title;
        $article->content = $request->content;
        $article->image = $request->image;
        $article->user_id =  $request->user_id;
        $article->category_id = $request->category_id;
        $article->save();

        return redirect()->route('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artikelUntukDiedit = DB::table('articles')->where('id',$id)->first();
        $daftarKategori = DB::table('categories')->orderBy('updated_at', 'desc')->get();
        return view('edit-article', ["artikelUntukDiedit"=>$artikelUntukDiedit,"daftarKategori"=>$daftarKategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);
        $article->title = $request->title;
        $article->content = $request->content;
        $article->image = $request->image;
        $article->user_id =  $request->user_id;
        $article->category_id = $request->category_id;
        $article->save();

        return redirect()->route('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::where('id',$id)->delete();
        return redirect()->route('dashboard');
    }
}

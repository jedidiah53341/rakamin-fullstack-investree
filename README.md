# fullstack-investree-rakamin-vix

Ini merupakan penugasan akhir untuk virtual internship program.

## Panduan Menjalankan
1. Seperti pada umumnya, setelah clone, buat file .env dan buat APP_KEY dengan perintah 'php artisan key:generate'. Kemudian, buat database dengan nama 'fullstack_investree_rakamin_vix' sesuai dengan nilai DB_DATABASE, kemudian jalankan 'php artisan migrate:fresh --seed'.
3. Jalankan 'composer install' untuk download dependensi", kemudian 'php artisan serve' untuk menjalankan.

## Spesifikasi
#### Situs
1. Setelah menjalankan 'php artisan serve', browser akan menampilkan halaman root yang berisi daftar artikel dan kategori. Silahkan login atau register terlebih dahulu untuk mengakses menu CRUD.
![Welcome](welcome.jpg "Welcome")
2. Setelah login, anda tiba di dashboard. Pada dashboard, dibawah tabel artikel, anda dapat langsung memasukkan artikel baru ( formnya berwarna biru ), sedangkan dibawah tabel kategori, anda dapat langsung memasukkan kategori baru ( formnya berwarna hijau ).
![Dashboard](dashboard.jpg "Dashboard")
3. Semua kolom artikel dapat diedit kecuali penulisnya, sedangkan untuk kolom kategori, hanya nama kategori yang dapat diubah.
4. Relasi model dan pemanggilan data pada controller maupun template blade sudah menggunakan Eloquent.

#### API
5. Contoh register :
![register ](register.jpg "register ")
6. Contoh login :
![login](login.jpg "login")
7. Contoh list-all :
![list-all](list-all.jpg "list-all")
8. Contoh show-detail :
![show-detail](show-detail.jpg "show-detail")
9. Contoh update :
![update ](update.jpg "update ")
10. Contoh delete : 
![delete ](delete.jpg "delete ")

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Selamat datang di Dashboard. Disini, anda dapat mengolah artikel dan kategori artikel.<br/><br/>
            Seluruh artikel akan ditampilkan, tidak terbatas pada artikel yang akun anda buat saja -- demikian halnya dengan kategori.
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="table-responsive">
                    <div class="card bg-primary w-100 mt-2" >
                        <div class="card-body">
                            <form method="POST" action="{{ route('update-category',['id' => $kategoriUntukDiedit->id]) }}" class="forms-sample">
                            @method('PUT')
                            @csrf

                            <h5 class="card-title">Edit Kategori</h5>
                            <input name="name" type="text" class="form-control mt-3 mb-3" value="{{$kategoriUntukDiedit->name}}" />
                            <input name="user_name" type="text" class="form-control mt-3 mb-3" value="Penulis Artikel : {{ Auth::user()->name }}" disabled />
                            <input name="user_id" type="text" class="form-control mt-3 mb-3" value="{{$kategoriUntukDiedit->user_id}}" hidden />
                           
                            <button type="submit" class="bg-white btn btn-outline-primary w-100">Terapkan Edit</button>
                            </form>
                            <a href="{{ route('dashboard') }}"><button class="bg-white btn btn-outline-danger w-100">Tidak Jadi</button></a>
                        </div>
                     </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
<script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace('cKEditor');
</script>

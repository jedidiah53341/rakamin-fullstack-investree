<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Selamat datang di Dashboard. Disini, anda dapat mengolah artikel dan kategori artikel.<br/><br/>
            Seluruh artikel akan ditampilkan, tidak terbatas pada artikel yang akun anda buat saja -- demikian halnya dengan kategori.
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">

                <div class="p-6 bg-white border-b border-gray-200">
                    <span><strong>Daftar Artikel</strong></span>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Judul</th>
                                    <th>Konten</th>
                                    <th>URL Gambar</th>
                                    <th>Penulis</th>
                                    <th>Kategori</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                                    @if ($daftarArtikel->isEmpty())
                                    <tr>
                                    <td class="text-center" colspan="6"><i>Data artikel tidak ditemukan.</i></td>
                                    </tr>
                                    @endif

                                    @foreach ($daftarArtikel as $artikel)
                                    <tr>
                                    <td class="py-1"><span>{{$artikel->title}}</span></td>
                                    <td class="py-1"><span>{!! $artikel->content !!}</span></td>
                                    <td class="py-1"><span>{{$artikel->image}}</span></td>
                                    <td class="py-1">
                                        
                                    <span>{{ $artikel->user->name }}</span>

                                    </td>
                                    <td class="py-1">
                                        
                                    <span>{{ $artikel->category->name }}</span>



                                    </td>
                                    <td>
                                        <a class="text-white text-decoration-none" href="{{ route('edit-article',['id' => $artikel->id]) }}">
                                        <button type="button" class="mb-2 btn btn-xs btn-outline-success btn-rounded btn-icon">
                                        Edit
                                        </button>
                                        </a>

                                        <form class="d-inline" action="{{ route('delete-article',['id' => $artikel->id]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        <button type="submit" class="mb-2 btn btn-xs btn-outline-danger btn-rounded btn-icon">
                                            Hapus
                                        </button>
                                        </form>
                                    </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>


                    <div class="card bg-primary w-100 mt-2" >
                        <div class="card-body">
                            <form method="POST" action="{{ route('store-new-article') }}" class="forms-sample">
                            @csrf
                            <h5 class="card-title">Tulis Artikel Baru</h5>
                            <input name="title" type="text" class="form-control mt-3 mb-3" placeholder="Judul Artikel" />
                            <textarea id="cKEditor" name="content" class=" form-control" ></textarea>
                            <input name="image" type="text" class="form-control mt-3 mb-3" placeholder="URL File Gambar" />
                            <input name="user_name" type="text" class="form-control mt-3 mb-3" value="Penulis Artikel : {{ Auth::user()->name }}" disabled />
                            <input name="user_id" type="text" class="form-control mt-3 mb-3" value="{{Auth::user()->id}}" hidden />
                           
                            <select class="form-control mt-3 mb-3" name="category_id" >
                            <option disabled selected value="1" >Pilih Kategori</option>
                            @foreach ($daftarKategori as $kategori)
                            <option value="{{$kategori->id}}" >{{$kategori->name}}</option>
                            @endforeach
                            </select>
                            <button type="submit" class="bg-white btn btn-outline-primary w-100">Buat Artikel Baru</button>
                            </form>
                        </div>
                     </div>

                    <br/><br/>

                    <span><strong>Daftar Kategori</strong></span>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Pembuat Kategori</th>
                                    <th>Dibuat Pada</th>
                                    <th>Diupdate Pada</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>

                                    @if ($daftarKategori->isEmpty())
                                    <tr>
                                    <td class="text-center" colspan="6"><i>Data kategori tidak ditemukan.</i></td>
                                    </tr>
                                    @endif

                                    @foreach ($daftarKategori as $kategori)
                                    <tr>
                                    <td class="py-1"><span>{{$kategori->name}}</span></td>
                                    <td class="py-1">
                                        
                                    <span>{{ $kategori->user->name }}</span>

                                    </td>
                                    <td class="py-1"><span>{{$kategori->created_at}}</span></td>
                                    <td class="py-1"><span>{{$kategori->updated_at}}</span></td>
                                    <td>
                                        <a class="text-white text-decoration-none" href="{{ route('edit-category',['id' => $kategori->id]) }}">
                                        <button type="button" class="mb-2 btn btn-xs btn-outline-success btn-rounded btn-icon">
                                            Edit
                                        </button>
                                        </a>

                                        <form class="d-inline" action="{{ route('delete-category',['id' => $kategori->id]) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        <button type="submit" class="mb-2 btn btn-xs btn-outline-danger btn-rounded btn-icon">
                                            Hapus
                                        </button>
                                        </form>
                                    </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>

                        <div class="card bg-success w-100 mt-2" >
                        <div class="card-body">
                            <form method="POST" action="{{ route('store-new-category') }}" class="forms-sample">
                            @csrf
                            <h5 class="card-title">Tulis Kategori Baru</h5>
                            <input name="categoryName" type="text" class="form-control mt-3 mb-3" placeholder="Nama Kategori" />
                            <input name="user_name" type="text" class="form-control mt-3 mb-3" value="Pembuat Kategori : {{ Auth::user()->name }}" disabled />
                            <input name="user_id" type="text" class="form-control mt-3 mb-3" value="{{Auth::user()->id}}" hidden />
                            <button type="submit" class="bg-white btn btn-outline-primary w-100">Buat Kategori Baru</button>
                            </form>
                        </div>
                     </div>

                </div>

            </div>
        </div>
    </div>
</x-app-layout>
<script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace('cKEditor');
</script>

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\ArticleController@welcome')->name('welcome');

Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', 'App\Http\Controllers\ArticleController@index')->name('dashboard');

    // Route -route untuk artikel:
    // Tambah baru
    Route::post('/store-new-article', 'App\Http\Controllers\ArticleController@store')->name('store-new-article');
    // Halaman Edit
    Route::get('/edit-article/{id}', 'App\Http\Controllers\ArticleController@edit')->name('edit-article');
    // Terapkan Edit
    Route::put('/update-article/{id}', 'App\Http\Controllers\ArticleController@update')->name('update-article');
    // Hapus
    Route::delete('/delete-article/{id}', 'App\Http\Controllers\ArticleController@destroy')->name('delete-article');

    // Route -route untuk kategori:
    // Tambah baru
    Route::post('/store-new-category', 'App\Http\Controllers\CategoryController@store')->name('store-new-category');
    // Halaman Edit
    Route::get('/edit-category/{id}', 'App\Http\Controllers\CategoryController@edit')->name('edit-category');
    // Terapkan Edit
    Route::put('/update-category/{id}', 'App\Http\Controllers\CategoryController@update')->name('update-category');
    // Hapus
    Route::delete('/delete-category/{id}', 'App\Http\Controllers\CategoryController@destroy')->name('delete-category');
    
});


require __DIR__.'/auth.php';

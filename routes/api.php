<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
    'prefix' => ''
], function ($router) {

    // Route untuk API untuk artikel
    Route::post('/create', 'App\Http\Controllers\ArticleAPIController@store');
    Route::get('/list-all', 'App\Http\Controllers\ArticleAPIController@index');
    Route::get('/show-detail/{id}', 'App\Http\Controllers\ArticleAPIController@show');
    Route::put('/update/{id}', 'App\Http\Controllers\ArticleAPIController@update');
    Route::delete('/delete/{id}', 'App\Http\Controllers\ArticleAPIController@destroy');

    // Route untuk API autentikasi
    Route::post('/login', 'App\Http\Controllers\AuthController@login');
    Route::post('/register', 'App\Http\Controllers\AuthController@register');
    Route::post('/logout', 'App\Http\Controllers\AuthController@logout');
    Route::post('/refresh', 'App\Http\Controllers\AuthController@refresh');
    Route::get('/user-profile', 'App\Http\Controllers\AuthController@userProfile');    
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
